import Person from "./person.mjs";

class Speaker extends Person {
    say(phrase) {
        console.log(`${super.say(phrase)} very confidently`)
    }
}

let john = new Person('John', 'Dallaz');
console.log(john.say('Hello!'));

let bob = new Speaker('Bob', 'Malkovich');
console.log(bob.say('Hi!'));

let mark = new Person('Mark');
console.log(mark.question('some beer'));
